

$(document).on('change', '.input-group-btn :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

jQuery(document).ready(function($) {
	
	$('.input-group-btn :file').on('fileselect', function(event, numFiles, label) {
	    
	    var input = $(this).parents('.input-group').find(':text'),
	        log = numFiles > 1 ? numFiles + ' files selected' : label;
	    
	    if( input.length ) {
	        input.val(log);
	    } else {
	        if( log ) alert(log);
	    }
	    
	});
	

	$('.single .ui.dropdown').dropdown();


	$('.multiple-tag .ui.dropdown').dropdown();

	$('.menu-style .ui.dropdown').dropdown();

	
	$('.selection .ui.dropdown').dropdown();

	$('[data-toggle="popover"]').popover({ trigger: "hover" }); 


	$('.search-filter .dropdown-menu').on('click', function(event) {
		 event.stopPropagation();
		/* Act on the event */
	});

	
	if ($('.textarea').length) {
		jQuery('.textarea').wysihtml5();		
	};
	

		if ($("#dateField").length) {
				$("#dateField").datetimepicker({
				              minView: 2,
			  				format: 'dd-MM-yyyy'
				        });	
		};
		
		
		if ($('#daterange').length) {
			$('#daterange').daterangepicker({
				  "opens": "right"
			});
		};

			if ($(".dateField").length) {
						$(".dateField").datetimepicker({
					              minView: 2,
				  				format: 'dd-MM-yyyy'
					        });
			};
		
			if ($('#daterange').length) {
					$('#daterange').daterangepicker({
					  "opens": "right"
				});

			};



		if ($("#dateField-end").length) {
				$("#dateField-end").datetimepicker({
				              minView: 2,
			  				format: 'dd-MM-yyyy'
				        });

		};
		
		if ($('#daterange').length) {
				$('#daterange').daterangepicker({
				  "opens": "right"
			});

		};


		if ($('.timepicker').length) {
				$('.timepicker').datetimepicker({
			                format: 'HH:ii p',
			                autoclose: true,
			                // todayHighlight: true,
			                showMeridian: true,
			                startView: 1,
			                maxView: 1
			            });
		};


	// $('.search-filter .dropdown-menu').click(function(event){
	//      event.stopPropagation();
	//  });​

	
	
});//on laod


  

